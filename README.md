## This project is made for to showcase my Android coding skills.

Project Description of 20180413_SS_NycSchools(Date_initials_projectName)
This sample application is made to show case my coding skills on Android programming, and to showcase that I can use Major open source libries and framework along with MVVM and other design patterns. Also, this application showcase that I can write client-server application in very clean manner. Application consist of two screens(1 Activity which consist 2 fragments), first screen shows the list of schools in NYC with their basic information. The second screen shows selected school's SAT SCORE in formation and a way to contact the school)
Below are the major components and frameworks used.

### Design Pattern Used 
* MVVM Application Architecture
* Observer Pattern for callbacks.
* Singleton Pattern for ApiClient
### Unit testing
* Sample JUNIT test cases added for ViewModel class
* Sample Instrumentation test cases added for UI testing using Espresso framework
### Libraries Used
* Support library for Recycler View, CardView and Constraint Layout
* Retrofit and OK HTTP for Client-Server communication.
* GSON for data parsing and serialization.
* Android DataBinding library for data Binding.

### API Used
* Rest client with get api calls.
* NYC School districts open source APIS are being used to fetch and show data on UI.
* Link - https://opendata.cityofnewyork.us/how-to/#apidocumentation

### Assumption Made
* ApiClient class is handling both the api calls and NETWORK(Retrofit/OKHTTP) configuration. No network Manager is created considering the use case.
* Some error messages are shown using the Toast with hardcoded strings. These can be placed in string resources.
* Very basic info is being shown on the UI.
* If there is any data error, a tost/popup message is being shown, toast message disappers in few seconds.
* Progress dialog is being shown at the time of data loading.
* ToolBar animation is added.
* This app is running on SingleActivity with two fragments.

### Material Designs used
* CardView
* ConstraintLayout
* Coordinator layout with AppBarLayout and ToolBar
* RecyclerView and NestedScrollView


#### Contact Info

Please reach out to Me on the email ID from my resume.
