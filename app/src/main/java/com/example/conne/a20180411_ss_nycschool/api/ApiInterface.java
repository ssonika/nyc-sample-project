package com.example.conne.a20180411_ss_nycschool.api;

import com.example.conne.a20180411_ss_nycschool.model.School;
import com.example.conne.a20180411_ss_nycschool.model.SchoolInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    //Api call to get the list of schools
    @GET("97mf-9njv.json")
    Call<List<School>> getSchools();

    //Api call to get the SAT score for a perticular school
    @GET("734v-jeq5.json")
    Call<List<SchoolInfo>> getSchoolInfo(@Query("dbn") String dbn);
}
