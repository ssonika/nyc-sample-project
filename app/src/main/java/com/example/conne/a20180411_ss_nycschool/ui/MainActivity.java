package com.example.conne.a20180411_ss_nycschool.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.conne.a20180411_ss_nycschool.R;

/**
 * MainActivity for presenting the UI to the user
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        addFragment(new SchoolFragment(), getResources().getString(R.string.school_fragment));
    }

    /**
     * Method for adding the fragment into the activity
     *
     * @param fragment
     */
    public void addFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, tag).addToBackStack(null).commit();
    }

    /**
     * Method to remove fragment from back stack
     */
    public void removeFragment() {
        try {
            getSupportFragmentManager().popBackStackImmediate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Back press of the activity
     */
    @Override
    public void onBackPressed() {
        Fragment loadedFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (loadedFragment instanceof SchoolFragment) {
            finish();
            return;
        }
        super.onBackPressed();
    }
}
