package com.example.conne.a20180411_ss_nycschool.listener;

import com.example.conne.a20180411_ss_nycschool.model.School;

import java.util.List;

/**
 * Callback for the response of get list of schools api
 */
public interface SchoolsResponseListener {
    void getResponse(Object response);

    void getError(Throwable e);
}
