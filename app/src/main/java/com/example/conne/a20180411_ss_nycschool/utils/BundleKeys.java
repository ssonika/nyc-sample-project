package com.example.conne.a20180411_ss_nycschool.utils;

public interface BundleKeys {
    String SCHOOL_NAME = "school_name";
    String SCHOOL_EMAIL = "school_email";
    String SCHOOL_WEBSITE = "website";
}
