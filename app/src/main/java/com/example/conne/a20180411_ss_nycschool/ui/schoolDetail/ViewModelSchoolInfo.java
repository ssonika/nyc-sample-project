package com.example.conne.a20180411_ss_nycschool.ui.schoolDetail;

import android.view.View;

import com.example.conne.a20180411_ss_nycschool.model.SchoolInfo;

public class ViewModelSchoolInfo {
    private SchoolInfo model;
    private String DEFAULT_VALUE = "";
    private SchoolDetailFragment fragment;

    public ViewModelSchoolInfo(SchoolInfo schoolInfo, SchoolDetailFragment fragment) {
        model = schoolInfo;
        this.fragment = fragment;
    }

    public void setData(SchoolInfo schoolInfo) {
        model = schoolInfo;
    }

    public String getNumOfTestTaker() {
        if (!isValid()) {
            return DEFAULT_VALUE;
        }
        return model.getNumOfSatTestTakers();
    }

    public String getSatMathScore() {
        if (!isValid()) {
            return DEFAULT_VALUE;
        }
        return model.getSatMathAvgScore();
    }

    public String getSatCriticalReadingScore() {
        if (!isValid()) {
            return DEFAULT_VALUE;
        }
        return model.getSatCriticalReadingAvgScore();
    }

    public String getSatWritingScore() {
        if (!isValid()) {
            return DEFAULT_VALUE;
        }
        return model.getSatWritingAvgScore();
    }

    public String getSchoolName() {
        if (!isValid()) {
            return DEFAULT_VALUE;
        }
        return model.getSchoolName();
    }

    public void visitWebsiteClick(View view) {
        if(fragment !=null){
            fragment.visitWebsiteClick(view);
        }
    }

    public void sendEmailClick(View view) {
        if(fragment !=null){
            fragment.sendEmailClick(view);
        }
    }

    private boolean isValid() {
        return model != null;
    }
}
