package com.example.conne.a20180411_ss_nycschool.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.conne.a20180411_ss_nycschool.R;
import com.example.conne.a20180411_ss_nycschool.databinding.SchoolListItemBinding;
import com.example.conne.a20180411_ss_nycschool.model.School;

import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder> {

    private List<School> schoolList;
    private HomeListCallback callback;

    public SchoolListAdapter(SchoolFragment context, List<School> schoolList, HomeListCallback callBack) {
        this.schoolList = schoolList;
        this.callback = callBack;
    }

    static class SchoolViewHolder extends RecyclerView.ViewHolder {
        SchoolListItemBinding binding;

        public SchoolViewHolder(SchoolListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.school_list_item, parent, false);
        SchoolListItemBinding binding = DataBindingUtil.bind(view);
        return new SchoolListAdapter.SchoolViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        SchoolListItemBinding binding = holder.binding;
        binding.setSchool(schoolList.get(position));
        binding.setSchoolListAdapter(SchoolListAdapter.this);
        binding.executePendingBindings();
        binding.listItem.setTag(schoolList.get(position));
    }

    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    public void onItemClick(View view) {
        if (callback != null) {
            callback.onItemSelected((School) view.getTag());
        }
    }
}
