package com.example.conne.a20180411_ss_nycschool.ui.schoolDetail;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.conne.a20180411_ss_nycschool.R;
import com.example.conne.a20180411_ss_nycschool.api.ApiClient;
import com.example.conne.a20180411_ss_nycschool.databinding.SchoolDetailFragmentBinding;
import com.example.conne.a20180411_ss_nycschool.listener.SchoolsResponseListener;
import com.example.conne.a20180411_ss_nycschool.model.SchoolInfo;
import com.example.conne.a20180411_ss_nycschool.ui.MainActivity;
import com.example.conne.a20180411_ss_nycschool.utils.BundleKeys;
import com.example.conne.a20180411_ss_nycschool.utils.DialogUtil;

/**
 * Fragment to show the school's sat scores
 */
public class SchoolDetailFragment extends Fragment implements SchoolsResponseListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ApiClient apiClient;
    private SchoolDetailFragmentBinding binding;

    public static SchoolDetailFragment getInstance(String schoolName, String schoolEmail, String schoolWebsite) {
        SchoolDetailFragment fragment = new SchoolDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BundleKeys.SCHOOL_NAME, schoolName);
        bundle.putString(BundleKeys.SCHOOL_EMAIL, schoolEmail);
        bundle.putString(BundleKeys.SCHOOL_WEBSITE, schoolWebsite);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.school_detail_fragment, container, false);
        binding = DataBindingUtil.bind(view);
        getActivity().setTitle(R.string.school_detail_fragment);
        initComponent();
        return view;
    }

    private void initComponent() {
        Bundle bundle = getArguments();
        if (bundle == null || TextUtils.isEmpty(bundle.getString(BundleKeys.SCHOOL_NAME))) {
            Toast.makeText(getContext(), "There is a problem in getting the detail, please try again later.", Toast.LENGTH_LONG).show();
            return;
        }
        String schoolName = bundle.getString(BundleKeys.SCHOOL_NAME);
        apiClient = ApiClient.getInstance();
        apiClient.getSchoolDetailInfo(this, schoolName);
        DialogUtil.showDialog(getContext());
    }

    @Override
    public void getResponse(Object response) {
        DialogUtil.dismissDialog();
        if (binding == null) {
            return;
        }
        SchoolInfo schoolInfo = (SchoolInfo) response;
        binding.setViewModel(new ViewModelSchoolInfo(schoolInfo, this));
    }

    @Override
    public void getError(Throwable e) {
        Toast.makeText(getContext(), getString(R.string.error_message), Toast.LENGTH_LONG).show();
        DialogUtil.dismissDialog();
        //assuming parent activity is MainActivity for now.
        if (getActivity() != null) {
            ((MainActivity) getActivity()).removeFragment();
        }
    }

    public void visitWebsiteClick(View view) {
        Bundle bundle = getArguments();
        if (bundle == null || TextUtils.isEmpty(bundle.getString(BundleKeys.SCHOOL_WEBSITE))) {
            Toast.makeText(getContext(), "Error..", Toast.LENGTH_LONG).show();
            return;
        }
        String website = bundle.getString(BundleKeys.SCHOOL_WEBSITE);
        if (!website.startsWith("http://") && !website.startsWith("https://")) {
            website = "http://" + website;
        }

        Uri uriUrl = Uri.parse(website);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        if (getActivity() == null) {
            return;
        }
        try {
            startActivity(launchBrowser);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), "App can not handle this website, please try again later..", Toast.LENGTH_LONG).show();
        }
    }

    public void sendEmailClick(View view) {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        Bundle bundle = getArguments();
        if (bundle == null || TextUtils.isEmpty(bundle.getString(BundleKeys.SCHOOL_EMAIL))) {
            Toast.makeText(getContext(), "Error..", Toast.LENGTH_LONG).show();
            return;
        }
        String schoolEmail = bundle.getString(BundleKeys.SCHOOL_EMAIL);
        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{schoolEmail});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Sample Subject");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Sample body..");

        /* Send it off to the Activity-Chooser */
        if (getActivity() == null) {
            return;
        }

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "App can not handle this email, please try again later..", Toast.LENGTH_LONG).show();
        }
    }
}
