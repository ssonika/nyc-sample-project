package com.example.conne.a20180411_ss_nycschool.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogUtil {
    public static ProgressDialog mLoadingProgressDialog;

    public static void showDialog(Context context) {
        try {
            if (mLoadingProgressDialog == null && context != null) {
                mLoadingProgressDialog = android.app.ProgressDialog.show(context, null, "Loading ...", true, false);
            } else if (context != null && mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissDialog() {
        try {
            if (mLoadingProgressDialog != null && mLoadingProgressDialog.isShowing()) {
                mLoadingProgressDialog.dismiss();
                mLoadingProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
