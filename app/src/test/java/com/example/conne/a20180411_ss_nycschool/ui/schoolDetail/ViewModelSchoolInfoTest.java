package com.example.conne.a20180411_ss_nycschool.ui.schoolDetail;

import com.example.conne.a20180411_ss_nycschool.model.SchoolInfo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * adding sample test for ViewModel School Detail
 */
public class ViewModelSchoolInfoTest {
    ViewModelSchoolInfo viewModelSchoolInfo;

    @Before
    public void setup() {
        SchoolInfo info = new SchoolInfo();
        info.setDbn("TestDBN");
        info.setSchoolName("TestSchoolName");
        info.setSatMathAvgScore("100");
        info.setSatCriticalReadingAvgScore("100");
        info.setNumOfSatTestTakers("200");
        viewModelSchoolInfo = new ViewModelSchoolInfo(info, null);
    }

    @Test
    public void testSchoolNameNotNull() {
        Assert.assertEquals("TestSchoolName", viewModelSchoolInfo.getSchoolName());
    }

    @Test
    public void testTestTakersNumber() {
        Assert.assertEquals("200", viewModelSchoolInfo.getNumOfTestTaker());
    }
}
