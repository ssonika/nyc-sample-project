package com.example.conne.a20180411_ss_nycschool.ui;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.example.conne.a20180411_ss_nycschool.R;
import com.example.conne.a20180411_ss_nycschool.ui.schoolDetail.SchoolDetailFragment;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class SchoolDetailFragmentTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private SchoolDetailFragment schoolDetailFragment;

    public SchoolDetailFragmentTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        schoolDetailFragment = new SchoolDetailFragment();
        getActivity().addFragment(schoolDetailFragment, SchoolDetailFragment.class.getSimpleName());
        getInstrumentation().waitForIdleSync();
    }

    @Test
    public void testViewsDisplay() {

        Espresso.onView(ViewMatchers.withId(R.id.title_textView)).
                check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }
}
