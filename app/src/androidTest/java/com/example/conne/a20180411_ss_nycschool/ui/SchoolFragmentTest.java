package com.example.conne.a20180411_ss_nycschool.ui;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;

import com.example.conne.a20180411_ss_nycschool.R;

import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class SchoolFragmentTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mainActivity;

    public SchoolFragmentTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
    }

    public Fragment startFragment(Fragment fragment) {
        FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment, "tag");
        transaction.commit();
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();
        Fragment frag = mainActivity.getSupportFragmentManager().findFragmentByTag("tag");
        return frag;
    }

    @Test
    public void testStartSchoolFragment() {
        SchoolFragment schoolFragment = new SchoolFragment();
        startFragment(schoolFragment);
    }

}
